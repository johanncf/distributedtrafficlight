# Getting Started

The project contains several classes that can be run in a standalone or orchestrated ways.

## Executable Classes

### TrafficLightTimerMachine 

Central scheduler for synchronisation of the traffic lights. It waits for the traffic lights to register over the network with their ID over the port 1025.

### TrafficLightControllerMachine

Instance of a traffic light. It can either be simulated by a visaulization on the screen or by accessing the LEDs on the extension board.

**Arguments:**

- hostname / IP address of the `TrafficLightTimerMachine` instance (127.0.0.1 for localhost)
- ID of the traffic light (1-3)

### MultiThreadedSystem

This programm is a standalone simulation of the traffic light and instancieates the classes above internally in four different threads.

## Deployment and execution

### Copy JAR file to the PI

`scp distributedtrafficlight.jar pi@192.168.0.198:~/`

### Run the JAR file on the PI

`sudo java -jar distributedtrafficlight.jar`
