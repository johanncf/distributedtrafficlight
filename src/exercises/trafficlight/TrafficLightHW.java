package exercises.trafficlight;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;

public class TrafficLightHW extends TrafficLight {

	// create gpio controller instance
	final GpioController gpio = GpioFactory.getInstance();

	GpioPinDigitalOutput red;
	GpioPinDigitalOutput green;
	GpioPinDigitalOutput yellow;
	

	public TrafficLightHW(String title, boolean showYellow, Pin redPin, Pin yellowPin, Pin greenPin){
		super(title, showYellow);
		
		red = gpio.provisionDigitalOutputPin(redPin, "Red LED", PinState.LOW);
		green = gpio.provisionDigitalOutputPin(greenPin, "Green LED", PinState.LOW);
		yellow = gpio.provisionDigitalOutputPin(yellowPin, "Yellow LED", PinState.LOW);
		
		green.low();
		yellow.low();
		red.low();
	}
	
	public void showGreen() {
		red.low();
		if(yellow!=null) yellow.low();
		green.high();
	}

	public void showRed() {
		red.high();
		if(yellow!=null) yellow.low();
		green.low();
	}

	public void showRedYellow() {
		if(yellow==null) {
			throw new UnsupportedOperationException("Traffic light has no yellow light.");
		}
		red.high();
		yellow.high();
		green.low();
	}

	public void showYellow() {
		if(yellow==null) {
			throw new UnsupportedOperationException("Traffic light has no yellow light.");
		}
		red.low();
		yellow.high();
		green.low();
	}

	public void switchAllOff() {
		red.low();
		if(yellow!=null) yellow.low();
		green.low();
	}
}     

