package exercises.trafficlight;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import runtime.IStateMachine;
import runtime.Scheduler;
import runtime.Timer;

public class TrafficLightTimerMachine implements IStateMachine {

    private static final String Timer_1 = "t1", START = "Start", 
                                STOP = "Stop", GLOBAL_TICK = "GlobalTick";

    private static final int _30_sec_ = 20000;

    private enum STATES {
        INIT, TRAFFICLIGHT_1, TRAFFICLIGHT_2
    }

    private Timer t1 = new Timer(Timer_1);

    private Scheduler trafficLight1Scheduler;
    private Scheduler trafficLight2Scheduler;
    private Scheduler trafficLight3Scheduler;

    private static Socket clientSocket1;
    private static Socket clientSocket2;
    private static Socket clientSocket3;
    private static PrintWriter tl1;
    private static PrintWriter tl2;
    private static PrintWriter tl3;

    public TrafficLightTimerMachine() {
    }

    public TrafficLightTimerMachine(Scheduler trafficLight1Scheduler,
            Scheduler trafficLight2Scheduler,
            Scheduler trafficLight3Scheduler) {
        this.trafficLight1Scheduler = trafficLight1Scheduler;
        this.trafficLight2Scheduler = trafficLight2Scheduler;
        this.trafficLight3Scheduler = trafficLight3Scheduler;
    }

    protected STATES state = STATES.INIT;

    public int fire(String event, Scheduler scheduler) {
        if (state == STATES.INIT) {
            if (event.equals(START)) {
                t1.start(scheduler, _30_sec_);
                state = STATES.TRAFFICLIGHT_1;
                return EXECUTE_TRANSITION;
            }
        } else if (state == STATES.TRAFFICLIGHT_1) {
            if (event.equals(Timer_1)) {
                t1.start(scheduler, _30_sec_);
                state = STATES.TRAFFICLIGHT_2;
                // signal to the trafficlights 1 and 2
                if (this.trafficLight1Scheduler != null) {
                    this.trafficLight1Scheduler.addToQueueLast(GLOBAL_TICK);
                }
                if (this.trafficLight2Scheduler != null) {
                    this.trafficLight2Scheduler.addToQueueLast(GLOBAL_TICK);
                }
                if (TrafficLightTimerMachine.tl1 != null) {
                    tl1.println(GLOBAL_TICK);
                }
                if (TrafficLightTimerMachine.tl2 != null) {
                    tl2.println(GLOBAL_TICK);
                }
                return EXECUTE_TRANSITION;
            } else if (event.equals(STOP)) {
                t1.stop();
                state = STATES.INIT;
                return EXECUTE_TRANSITION;
            }
        } else if (state == STATES.TRAFFICLIGHT_2) {
            if (event.equals(Timer_1)) {
                t1.start(scheduler, _30_sec_);
                state = STATES.TRAFFICLIGHT_1;
                // signal to the trafficlight 3
                if (this.trafficLight2Scheduler != null) {
                    this.trafficLight3Scheduler.addToQueueLast(GLOBAL_TICK);
                }
                if (TrafficLightTimerMachine.tl3 != null) {
                    tl3.println(GLOBAL_TICK);
                }
                return EXECUTE_TRANSITION;
            } else if (event.equals(STOP)) {
                t1.stop();
                state = STATES.INIT;
                return EXECUTE_TRANSITION;
            }
        }
        return DISCARD_EVENT;
    }

    private static void setupClients(int portNumber) {
        try {
            ServerSocket serverSocket = new ServerSocket(portNumber);
            // portNumber: An integer containing a port number above 1024 to be accessed by the clients

            // Wait for a call from a client
            clientSocket1 = serverSocket.accept();
            System.out.println("client 1 connected");

            BufferedReader in1 = new BufferedReader(new InputStreamReader(clientSocket1.getInputStream()));
            PrintWriter out1 = new PrintWriter(clientSocket1.getOutputStream(), true);
            switch (in1.readLine()) {
                case "1": tl1 = out1; break;
                case "2": tl2 = out1; break;
                case "3": tl3 = out1; break;
                default: System.out.println("Error: Could not assign client 1 to traffic light!");break;
            }

            clientSocket2 = serverSocket.accept();
            System.out.println("client 2 connected");

            BufferedReader in2 = new BufferedReader(new InputStreamReader(clientSocket2. getInputStream()));
            PrintWriter out2 = new PrintWriter(clientSocket2.getOutputStream(), true);
            switch (in2.readLine()) {
                case "1": tl1 = out2; break;
                case "2": tl2 = out2; break;
                case "3": tl3 = out2; break;
                default: System.out.println("Error: Could not assign client 2 to traffic light!");break;
            }

            clientSocket3 = serverSocket.accept();
            System.out.println("client 3 connected");

            BufferedReader in3 = new BufferedReader(new InputStreamReader(clientSocket3. getInputStream()));
            PrintWriter out3 = new PrintWriter(clientSocket3.getOutputStream(), true);
            switch (in3.readLine()) {
                case "1": tl1 = out3; break;
                case "2": tl2 = out3; break;
                case "3": tl3 = out3; break;
                default: System.out.println("Error: Could not assign client 3 to traffic light!");break;
            }
    
        } catch (IOException e) { 
            System.out.println("Exception caught when trying to listen on port " + portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
        IStateMachine stm = new TrafficLightTimerMachine();
        Scheduler s = new Scheduler(stm);

        setupClients(1025);

        System.out.println("Start statemachine...");

        s.start();

        stm.fire(START, s);
    }
}
