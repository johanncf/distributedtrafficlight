package exercises.trafficlight;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import com.pi4j.io.gpio.RaspiPin;
import runtime.EventWindow;
import runtime.IStateMachine;
import runtime.Scheduler;
import runtime.Timer;

public class TrafficLightControllerMachine implements IStateMachine {

	private static final String PEDESTRIAN_BUTTON_PRESSED = "Pedestrian Button",
			Timer_1 = "t1", Timer_2 = "t2", Timer_3 = "t3",
			Timer_4 = "t4", Timer_5 = "t5", GLOBAL_TICK = "GlobalTick";

	private boolean pedestrian_button_was_pressed = false;

	public static final String[] EVENTS = { PEDESTRIAN_BUTTON_PRESSED };

	private enum STATES {
		S0, S1, S2, S3, S4, S5, S6
	}

	private Timer t1 = new Timer(Timer_1);
	private Timer t2 = new Timer(Timer_2);
	private Timer t3 = new Timer(Timer_3);
	private Timer t4 = new Timer(Timer_4);
	private Timer t5 = new Timer(Timer_5);
	//private Timer t6 = new Timer(Timer_6);

	protected STATES state = STATES.S6;

	private TrafficLight cars = new TrafficLight("Cars", true);
	private TrafficLight pedestrians = new TrafficLight("Pedestrians", false);
	//private TrafficLightHW cars = new TrafficLightHW("Cars", true,
	//													RaspiPin.GPIO_07, RaspiPin.GPIO_03,
	//													RaspiPin.GPIO_13);
	//private TrafficLightHW pedestrians = new TrafficLightHW("Pedestrians", false, 
	//														RaspiPin.GPIO_00, RaspiPin.GPIO_12,
	//														RaspiPin.GPIO_14);

	public TrafficLightControllerMachine() {
		// initial transition
		cars.setVisible(true);
		pedestrians.setVisible(true);
		cars.showGreen();
		pedestrians.showRed();
	}

	public int fire(String event, Scheduler scheduler) {
		if (state == STATES.S0) {
			if (event.equals(PEDESTRIAN_BUTTON_PRESSED)) {
				cars.showYellow();
				t1.start(scheduler, 1000);
				state = STATES.S1;
				return EXECUTE_TRANSITION;
			}
		} else if (state == STATES.S1) {
			pedestrian_button_was_pressed = false;
			if (event.equals(Timer_1)) {
				cars.showRed();
				t2.start(scheduler, 500);
				state = STATES.S2;
				return EXECUTE_TRANSITION;
			}
		} else if (state == STATES.S2) {
			if (event.equals(Timer_2)) {
				pedestrians.showGreen();
				t3.start(scheduler, 3000);
				state = STATES.S3;
				return EXECUTE_TRANSITION;
			}
		} else if (state == STATES.S3) {
			if (event.equals(Timer_3)) {
				pedestrians.showRed();
				t4.start(scheduler, 1000);
				state = STATES.S4;
				return EXECUTE_TRANSITION;
			}
		} else if (state == STATES.S4) {
			if (event.equals(Timer_4)) {
				cars.showYellow();
				t5.start(scheduler, 1000);
				state = STATES.S5;
				return EXECUTE_TRANSITION;
			} else if (event.equals(PEDESTRIAN_BUTTON_PRESSED)) {
				pedestrian_button_was_pressed = true;
			}
		} else if (state == STATES.S5) {
			if (event.equals(PEDESTRIAN_BUTTON_PRESSED)) {
				pedestrian_button_was_pressed = true;
			} else if (event.equals(Timer_5)) {
				cars.showGreen();
				state = STATES.S6;
				return EXECUTE_TRANSITION;
			}
		} else if (state == STATES.S6) {
			if (event.equals(PEDESTRIAN_BUTTON_PRESSED)) {
				pedestrian_button_was_pressed = true;
			} else if (event.equals(GLOBAL_TICK)) {
				if (pedestrian_button_was_pressed) {
					cars.showYellow();
					t1.start(scheduler, 1000);
					state = STATES.S1;
				} else {
					state = STATES.S6;
				}
				return EXECUTE_TRANSITION;
			}
		}
		return DISCARD_EVENT;
	}

	public static void main(String[] args) {
		String host = args[0];
		String id = args[1];
		IStateMachine stm = new TrafficLightControllerMachine();
		Scheduler s = new Scheduler(stm);

		// ButtonHW button = new ButtonHW(RaspiPin.GPIO_11, EVENTS[0], s);
		String eventFromServer;
		int portNumber = 1025;

		EventWindow w = new EventWindow(EVENTS, s);
		w.show();

		s.start();

		try {
			Socket clientSocket = new Socket(host, portNumber);
			// hostname: A string containing the computer name or IP address of the server
			// portNumber: An integer containing a port number above 1024 supported by the server
	
			PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream ()));
			
			// Write string toServer to the server
			out.println(id);
			out.println(id); 
	
			// Continuously read the input from the connection, write a received string to variable fromServer,
			// and carry out doSomething(fromServer) afterwards.
			while ((eventFromServer = in.readLine()) != null) { 
				//doSomething(fromServer);
				s.addToQueueLast(eventFromServer);
			}
		} catch (IOException e) {
			System.out.println("Exception caught when trying to listen on port " + portNumber + " or listening for a connection");
			System.out.println(e.getMessage());
		}
	}

}
