package exercises.trafficlight;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import runtime.Scheduler;

public class ButtonHW {

	// create gpio controller instance
	final GpioController gpio = GpioFactory.getInstance();

	GpioPinDigitalInput pedestrianButton;

	public ButtonHW (Pin buttonPin, String event, Scheduler scheduler){
		
        GpioPinDigitalInput pedestrianButton = gpio.provisionDigitalInputPin(buttonPin, "Button", PinPullResistance.PULL_DOWN);
		
		// create and register gpio pin listener
		pedestrianButton.addListener(new ButtonListener(event, scheduler));
	}
	
	public static class ButtonListener implements GpioPinListenerDigital {
		private String buttonEvent;
		private Scheduler scheduler;
		
		public ButtonListener(String event, Scheduler scheduler) {
			this.buttonEvent = event;
			this.scheduler = scheduler;
		}
	    @Override
	    public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
	        // display pin state on console
	    	this.scheduler.addToQueueLast(buttonEvent);
	    }
	}
}     

