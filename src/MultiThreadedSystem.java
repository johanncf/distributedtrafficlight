import exercises.trafficlight.TrafficLightControllerMachine;
import exercises.trafficlight.TrafficLightTimerMachine;
import runtime.EventWindow;
import runtime.IStateMachine;
import runtime.Scheduler;

public class MultiThreadedSystem extends Thread {
    
    private static final String PEDESTRIAN_BUTTON_PRESSED = "Pedestrian Button", START = "START";
    public static final String[] EVENTS = { PEDESTRIAN_BUTTON_PRESSED };
    
    public static void main(String[] args) {
        TrafficLightControllerMachine trafficLight1 = new TrafficLightControllerMachine();
        TrafficLightControllerMachine trafficLight2 = new TrafficLightControllerMachine();
        TrafficLightControllerMachine trafficLight3 = new TrafficLightControllerMachine();

        Scheduler s1  = new Scheduler(trafficLight1);
        Scheduler s2  = new Scheduler(trafficLight2);
        Scheduler s3  = new Scheduler(trafficLight3);

        s1.start();
        EventWindow w1 = new EventWindow(EVENTS, s1);
		w1.show();

        s2.start();
        EventWindow w2 = new EventWindow(EVENTS, s2);
		w2.show();

        s3.start();
        EventWindow w3 = new EventWindow(EVENTS, s3);
		w3.show();

        IStateMachine stm = new TrafficLightTimerMachine(s1, s2, s3);
        Scheduler s = new Scheduler(stm);

        s.start();
        stm.fire(START, s);
    }
}
